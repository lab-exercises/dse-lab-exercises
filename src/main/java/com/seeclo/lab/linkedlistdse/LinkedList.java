package com.seeclo.lab.linkedlistdse;

public class LinkedList {

    private Node head;
    private Node tail;
    private int lenght;

    public class Node {
        public int value;
        public Node next;
        public Node(int value) {
            this.value = value;
        }
    }

    public LinkedList(int value) {
        Node newNode = new Node(value);
        head = newNode;
        tail = newNode;
        lenght = 1;
    }
    public void printList(){
        Node temp = head;
        while (temp != null){
            System.out.print(temp.value +" ");
            temp = temp.next;
        }
    }

    public void append(int value){
        Node newNode =  new Node(value);
        if(lenght==0){
            tail = newNode;
            head = newNode;
        }else{
            tail.next = newNode;
            tail = newNode;
        }
        lenght++;
    }

    public void prepend(int value){
        Node newNode = new Node(value);
        if(lenght==0){
            tail = newNode;
            head = newNode;
        }else{
            newNode.next = head;
            head = newNode;
        }
        lenght++;
    }

    public Node removeLastNode(){
        if(lenght<=0){
            return null;
        }else if(head == tail) {
            Node temp = head;
            head = null;
            tail = null;
            lenght --;
            return temp;
        } else{
            Node temp = head, pre = head;
            while (temp.next != null){
                pre = temp;
                temp = temp.next;
            }
            tail = pre;
            tail.next = null;
            lenght --;
            return temp;
        }
    }

    public Node removeFirstNode(){
        if(lenght==0){
            return null;
        }
        Node temp = head;
        head = head.next;
        temp.next = null;
        lenght --;

        if(lenght == 0){
            tail = null;
        }
        return temp;

    }

    public Node get(int index){
        if(index < 0 || index >= lenght){
            return null;
        }
        Node temp = head;
        for(int i = 0; i<index; i++) {
            temp = temp.next;
        }
        return temp;
    }

    public boolean insert(int index, int value){
        if(index < 0 || index >= lenght){
            return false;
        }
        if(index == 0){
            prepend(value);
            return true;
        }

        if(index == lenght){
            append(value);
            return true;
        }
        Node node = new Node(value);
        Node temp = get(index-1);
        node.next = temp.next;
        temp.next = node;
        lenght ++;
        return true;
    }

    public boolean set(int index, int value){
        Node node = get(index);
        if(node!=null) {
            node.value = value;
            return true;
        }
        return false;
    }


    public Node remove(int index){
        if(lenght == 0 || index < 0 || index >= lenght){
            return null;
        }

        if(index == 0){
            return removeFirstNode();
        }

        if(index == lenght-1){
            return removeLastNode();
        }

        Node prev = get(index -1);
        Node temp = prev.next;


        prev.next = temp.next;
        temp.next = null;
        lenght --;
        return temp;
    }


    public void reverse(){
        Node temp = head;
        head = tail;
        tail = temp;
        Node after = temp.next;
        Node before = null;
        for (int i = 0 ; i < lenght ; i++){
            after = temp.next;
            temp.next = before;
            before = temp;
            temp = after;
        }
    }

    public int getLenght() {
        return lenght;
    }
}

