package com.seeclo.lab.heapLabs;

import com.seeclo.lab.heapdse.Heap;

import java.util.ArrayList;
import java.util.List;

public class StreamMax {


    public static List<Integer> streamMax(int[] nums){
        List<Integer> maxNum = new ArrayList<>();
        int i = 0;
        Heap maxNumHeap = new Heap();
        for (int num : nums){
            maxNumHeap.insert(num);
             maxNum.add(maxNumHeap.getHeap().get(0));
        }
        return maxNum;
    }


    public static void main(String[] args) {
/*        // Test case 1
        int[] nums1 = {1, 5, 2, 9, 3, 6, 8};
        System.out.println("Test case 1:");
        System.out.println("Input: [1, 5, 2, 9, 3, 6, 8]");
        System.out.println("Expected output: [1, 5, 5, 9, 9, 9, 9]");
        System.out.println("Actual output: " + streamMax(nums1));
        System.out.println();

        // Test case 2
        int[] nums2 = {10, 2, 5, 1, 0, 11, 6};
        System.out.println("Test case 2:");
        System.out.println("Input: [10, 2, 5, 1, 0, 11, 6]");
        System.out.println("Expected output: [10, 10, 10, 10, 10, 11, 11]");
        System.out.println("Actual output: " + streamMax(nums2));
        System.out.println();

        // Test case 3
        int[] nums3 = {3, 3, 3, 3, 3};
        System.out.println("Test case 3:");
        System.out.println("Input: [3, 3, 3, 3, 3]");
        System.out.println("Expected output: [3, 3, 3, 3, 3]");
        System.out.println("Actual output: " + streamMax(nums3));
        System.out.println();*/

        StreamMax streamMax = new StreamMax();
        streamMax.extracted();

        /*
            EXPECTED OUTPUT:
            ----------------
            Test case 1:
            Input: [1, 5, 2, 9, 3, 6, 8]
            Expected output: [1, 5, 5, 9, 9, 9, 9]
            Actual output: [1, 5, 5, 9, 9, 9, 9]

            Test case 2:
            Input: [10, 2, 5, 1, 0, 11, 6]
            Expected output: [10, 10, 10, 10, 10, 11, 11]
            Actual output: [10, 10, 10, 10, 10, 11, 11]

            Test case 3:
            Input: [3, 3, 3, 3, 3]
            Expected output: [3, 3, 3, 3, 3]
            Actual output: [3, 3, 3, 3, 3]

        */

    }

    private void extracted() {
        Temp temp = new Temp();
        temp.setFullname("nitesh kumar n");
        Another another = new Another();

        another.setFirstname(temp.name);
        another.setFullname(temp.fullname);

        System.out.println(another.toString());
    }

    class Another{
        private String firstname;
        private String fullname;

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getFullname() {
            return fullname;
        }

        public void setFullname(String fullname) {
            this.fullname = fullname;
        }

        @Override
        public String toString() {
            return "Another{" +
                    "firstname='" + firstname + '\'' +
                    ", fullname='" + fullname + '\'' +
                    '}';
        }
    }


    class Temp{

        private String name;
        private String fullname;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getFullname() {
            return fullname;
        }

        public void setFullname(String fullname) {
            this.fullname = fullname;
        }

        @Override
        public String toString() {
            return "Temp{" +
                    "name='" + name + '\'' +
                    ", fullname='" + fullname + '\'' +
                    '}';
        }
    }

}
