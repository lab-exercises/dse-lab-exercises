package com.seeclo.lab.queuedse;

public class Queue {

    private Node first;
    private Node last;
    private int lenght;

    public Queue(int value) {
        Node node = new Node(value);
        first = node;
        last = node;
        lenght++;
    }

    class Node {

        public Node(int value) {
            this.value = value;
        }

        public Node next;
        public int value;
    }

    public int getFirst() {
        if(lenght == 0) return 0;
        return first.value;
    }

    public int getLast() {
        if(lenght == 0) return 0;
        return last.value;
    }

    public int getLenght() {
        return lenght;
    }

    public void printQueue(){
        if(lenght==0) return;
        Node node = first;
        while (node != null){
            System.out.println(node.value);
            node = node.next;
        }
    }

    public void enqueue(int value){
        Node node = new Node(value);
        if(lenght == 0){
            first = node;
            last = node;
        }else{
            last.next = node;
            last = node;
        }
        lenght++;
    }

    public void dequeue(){
        if(lenght == 0) return;
        Node temp = first;
        if(lenght == 1){
            first = null;
            last = null;
        }else {
            first = temp.next;
            temp.next = null;
        }
        lenght--;
    }
}
