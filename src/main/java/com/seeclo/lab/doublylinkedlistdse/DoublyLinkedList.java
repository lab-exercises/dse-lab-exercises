package com.seeclo.lab.doublylinkedlistdse;

public class DoublyLinkedList {

    private Node head;
    private Node tail;
    private int length;

    public DoublyLinkedList() {
    }

    public DoublyLinkedList(int value) {
        Node node = new Node(value);
        this.head = node;
        this.tail = node;
        this.length=1;
    }

    public class Node{
        public int value;
        private Node next;
        private Node prev;

        public Node(int value) {
            this.value = value;
        }
    }

    public void getHead(){
        if(head== null) return;
        System.out.println("the value of head :: "+ head.value);
    }

    public void getTail(){
        if(tail== null) return;
        System.out.println("the value of Tail :: "+ tail.value);
    }

    public void getLength(){
        System.out.println("The length of Doubly Linked List :: "+length);
    }

    public void append(int value){
        Node node = new Node(value);
        if(length == 0){
            head = node;
            tail = node;
        }else {
            tail.next = node;
            node.prev = tail;
            tail = node;
        }
        length++;
    }

    public void printList(){
        if(head == null) System.out.println("List is empty");

        Node temp = head;
        for (int i = 0 ; i < length; i++){
            System.out.println(temp.value);
            temp = temp.next;
        }
    }

    public Node removeLast(){
        if(head == null) return null;

        Node temp = tail;
        if(length == 1){
            tail = null;
            head = null;
        }else{
            tail = temp.prev;
            tail.next =  null;
            temp.prev = null;
        }
        length --;
        return temp;
    }

    public void prepend(int value){
        Node node = new Node(value);
        if(length == 0){
            head = node;
            tail = node;
        }else{
            node.next = head;
            head.prev = node;
            head = node;
        }
        length ++;
    }

    public Node removeFirst(){
        if(length == 0) return null;

        Node temp = head;
        if(length == 1){
            head = null;
            tail = null;
        }else{
            head = head.next;
            temp.next = null;
            head.prev = null;
        }
        length --;
        return temp;
    }

    public Node get(int index){
        if(index < 0 || index >= length) return null;

        if(index == 0) return head;

        if(index == length-1) return tail;

        int dis = length - index;
        Node temp = null;
        if(dis >= index){
            temp = head;
            for (int i = 0; i < index ; i++) {
                temp = temp.next;
            }
        }else{
            temp = tail;
            for (int i = length - 1 ; i > index ; i--){
                temp = temp.prev;
            }
        }
        return temp;
    }

    public boolean set(int index, int value){
        Node node = get(index);
        if(node!=null){
            node.value = value;
            return true;
        }
        return false;
    }

    public boolean insert(int index, int value){
        if(index < 0 || index > length) return false;

        if(index == 0){
            prepend(value);
            return true;
        }

        if(index == length){
            append(value);
            return true;
        }

        Node newNode = new Node(value);
        Node node = get(index);
        newNode.prev = node.prev;
        newNode.next = node;
        node.prev = newNode;
        newNode.prev.next = newNode;

        length++;

        return true;
    }

    public boolean remove(int index){
        if(index < 0 || index >= length) return false;

        if(index == 0){
            removeFirst();
            return true;
        }
        if(index == length-1){
            removeLast();
            return true;
        }

        Node node = get(index);
        Node before = node.prev;
        Node after = node.next;

        before.next = after;
        after.prev = before;
        node.prev = null;
        node.next = null;

        length --;
        return true;
    }
}
