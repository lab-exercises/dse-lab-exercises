package com.seeclo.lab.doublylinkedlistdse;

public class Main {
    public static void main(String[] args) {
        DoublyLinkedList doublyLinkedList = new DoublyLinkedList();
        doublyLinkedList.append(7);
        doublyLinkedList.prepend(5);
        doublyLinkedList.prepend(3);
        doublyLinkedList.prepend(1);
        doublyLinkedList.prepend(0);
        doublyLinkedList.append(9);
        doublyLinkedList.append(10);
        System.out.println(":: Printing doubly linked list ::");
        doublyLinkedList.getHead();
        doublyLinkedList.getTail();
        doublyLinkedList.printList();
        doublyLinkedList.getLength();

        doublyLinkedList.removeLast();
        doublyLinkedList.removeFirst();

        System.out.println(":: Printing doubly linked list ::");
        doublyLinkedList.printList();
        doublyLinkedList.getLength();

        System.out.println("Get the value of index :: "+doublyLinkedList.get(0).value);

        System.out.println("Get the value of index :: "+doublyLinkedList.get(1).value);

        System.out.println("Get the value of index :: "+doublyLinkedList.get(2).value);

        System.out.println("Get the value of index :: "+doublyLinkedList.get(3).value);

        System.out.println("Get the value of index :: "+doublyLinkedList.get(4).value);

        System.out.println("Get the value of index :: "+doublyLinkedList.get(5));
        System.out.println(":: Printing doubly linked list ::");
        doublyLinkedList.printList();
        doublyLinkedList.getLength();

        System.out.println(":: The set method to set the value in the index ::");
        System.out.println(doublyLinkedList.set(0, 11));
        System.out.println(doublyLinkedList.set(5, 11));
        doublyLinkedList.printList();
        doublyLinkedList.getLength();

        doublyLinkedList.insert(0, 15);
        doublyLinkedList.insert(6, 17);
        doublyLinkedList.insert(3, 19);
        doublyLinkedList.insert(8, 21);
        doublyLinkedList.printList();
        doublyLinkedList.getLength();

        System.out.println("remove 0 index :: "+ doublyLinkedList.remove(0));
        doublyLinkedList.printList();
        doublyLinkedList.getLength();
        System.out.println("remove 7 index :: "+ doublyLinkedList.remove(7));
        doublyLinkedList.printList();
        doublyLinkedList.getLength();
        System.out.println("remove 2 index :: "+ doublyLinkedList.remove(2));
        doublyLinkedList.printList();
        doublyLinkedList.getLength();
        System.out.println("remove 5 index :: "+ doublyLinkedList.remove(5));
        doublyLinkedList.printList();
        doublyLinkedList.getLength();
    }
}
