package com.seeclo.lab.binarysearchtreedse;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class BinarySearchRecursiveTree {

    Node root;

    class Node{

        public int value;

        public Node left;

        public Node right;

        public Node(int value) {
            this.value = value;
        }
    }

    public boolean rContains(int value){
        return rContains(root, value);
    }

    private boolean rContains(Node currentNode, int value){
        if(currentNode == null) return false;

        if(currentNode.value == value) return true;

        if(value < currentNode.value){
            return this.rContains(currentNode.left, value);
        }else{
            return this.rContains(currentNode.right, value);
        }
    }

    public void rInsert(int value){
        if(root == null) root = new Node(value);
        rInsert(root, value);
    }

    private Node rInsert(Node currentNode, int value){
        if(currentNode == null) return new Node(value);
        if(value < currentNode.value){
            currentNode.left = rInsert(currentNode.left, value);
        }else if(value > currentNode.value){
            currentNode.right = rInsert(currentNode.right, value);
        }
        return currentNode;
    }


    public void deleteNode(int value){
        deleteNode(root, value);
    }

    private Node deleteNode(Node currentNode, int value){
        if(currentNode == null) return null;

        if(value < currentNode.value){
            currentNode.left = deleteNode(currentNode.left, value);
        }else if(value > currentNode.value){
            currentNode.right = deleteNode(currentNode.right, value);
        }else{
            if(currentNode.left == null && currentNode.right == null){
                currentNode =  null;
            }else if(currentNode.left == null){
                currentNode = currentNode.right;
            }else if(currentNode.right == null){
                currentNode = currentNode.left;
            }else{
                int subTreeMin = minValue(currentNode.right);
                currentNode.value = subTreeMin;
                currentNode.right = deleteNode(currentNode.right, subTreeMin);
            }
        }
        return currentNode;
    }

    public int minValue(Node currentNode){
        while(currentNode.left != null){
            currentNode = currentNode.left;
        }
        return currentNode.value;
    }

    public boolean isBalanced(){
        return height(root) != -1;
    }

    public int height(Node node) {
        if (node == null) return 0;
        int leftHeight = height(node.left);
        if (leftHeight == -1) return -1;
        int rightHeight = height(node.right);
        if (rightHeight == -1) return -1;
        if (Math.abs(leftHeight - rightHeight) > 1) return -1;
        return 1 + Math.max(leftHeight, rightHeight);
    }

    public void sortedArrayToBST(int[] nums) {
        this.root = sortedArrayToBST(nums, 0, nums.length - 1);
    }

    private Node sortedArrayToBST(int[] nums, int left, int right){
        if(left > right) return null;
        int mid = left + (right - left)/2;

        Node node = new Node(nums[mid]);

        node.left = sortedArrayToBST(nums, left, mid -1);
        node.right = sortedArrayToBST(nums, mid + 1, right);
        return node;
    }
    public List<Integer> inorderTraversal() {
        List<Integer> result = new ArrayList<>();
        inorderHelper(this.root, result);
        return result;
    }

    private void inorderHelper(Node node, List<Integer> result) {
        if (node == null) return;
        inorderHelper(node.left, result);
        result.add(node.value);
        inorderHelper(node.right, result);
    }

    public Node invertTree(Node node){
        if(node == null) return null;

        node.left = invertTree(node.left);
        node.right = invertTree(node.right);


            Node temp = node.left;
            node.left = node.right;
            node.right = temp;


        return node;
    }

    public ArrayList<Integer> breathFirstSearchTree(){
        Node currentNode = root;
        Queue<Node> queue = new LinkedList<>();
        ArrayList<Integer> results = new ArrayList<>();

        queue.add(currentNode);
        while (queue.size() > 0) {
            currentNode = queue.remove();
            results.add(currentNode.value);
            if(currentNode.left != null){
                queue.add(currentNode.left);
            }
            if(currentNode.right != null){
                queue.add(currentNode.right);
            }
        }
        return results;
    }

}
