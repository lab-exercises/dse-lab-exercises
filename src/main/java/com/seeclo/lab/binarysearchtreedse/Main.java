package com.seeclo.lab.binarysearchtreedse;

public class Main {

    public static void main(String[] args) {
        BinarySearchTree binarySearchTree = new BinarySearchTree();

        BinarySearchRecursiveTree binarySearchRecursiveTree = new BinarySearchRecursiveTree();

        System.out.println("root = :: "+binarySearchTree.root);

        binarySearchTree.insert(47);
        binarySearchTree.insert(21);
        binarySearchTree.insert(78);
        binarySearchTree.insert(58);
        binarySearchTree.insert(82);
        binarySearchTree.insert(55);
        binarySearchTree.insert(27);

        System.out.println(binarySearchTree.root.left.right.value);
        System.out.println(binarySearchTree.root.right.left.left.value);
        System.out.println(binarySearchTree.contains(27));
        System.out.println(binarySearchTree.contains(17));
        System.out.println(binarySearchTree.contains(55));


        binarySearchRecursiveTree.rInsert(2);
        binarySearchRecursiveTree.rInsert(1);
        binarySearchRecursiveTree.rInsert(3);


        System.out.println("___________________________");
        System.out.println(binarySearchRecursiveTree.isBalanced());
        System.out.println(binarySearchRecursiveTree.height(binarySearchRecursiveTree.root));

        System.out.println("root ::"+ binarySearchRecursiveTree.root.value );
        System.out.println("left ::"+ binarySearchRecursiveTree.root.left.value );
        System.out.println("right ::"+ binarySearchRecursiveTree.root.right.value );

        System.out.println("___________________________");

        binarySearchRecursiveTree.deleteNode(1);


        System.out.println(binarySearchRecursiveTree.isBalanced());
        System.out.println(binarySearchRecursiveTree.height(binarySearchRecursiveTree.root));

        System.out.println("root ::"+ binarySearchRecursiveTree.root.value );
        System.out.println("right ::"+ binarySearchRecursiveTree.root.right.value );
        System.out.println("left ::"+ binarySearchRecursiveTree.root.left.value );

    }
}
