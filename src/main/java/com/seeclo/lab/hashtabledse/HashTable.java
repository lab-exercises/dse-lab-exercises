package com.seeclo.lab.hashtabledse;

import java.util.ArrayList;
import java.util.List;

public class HashTable {
    int size = 9 ;
    Node[] dataMap;

    public HashTable() {
        this.dataMap = new Node[size];
    }

    private int hash(String key){
        int hash = 0;
        char[] charKey = key.toCharArray();
        for(int i = 0 ; i < charKey.length ; i++){
            int asciiValue = charKey[i];
            hash = (hash + asciiValue * 23) % dataMap.length;
        }
        return hash;
    }

    public void set(String key, int value){
        int index =  hash(key);
        Node node = new Node(key, value);
        if(dataMap[index] == null) {
            dataMap[index] = node;
        }else{
            Node temp = dataMap[index];
            while(temp.next != null){
                temp = temp.next;
            }
            temp.next = node;
        }
    }

    public int get(String key){
        int index = hash(key);
        Node temp = dataMap[index];
        while (temp != null){
            if(temp.key.equals(key)){
                return temp.value;
            }
            temp = temp.next;
        }
        return 0;
    }

    public List<String> keys(){
        List<String> allKeys = new ArrayList<>();
        for(int i = 0 ; i < dataMap.length ; i++){
            Node temp = dataMap[i];
            while (temp != null){
                allKeys.add(temp.key);
                temp = temp.next;
            }
        }
        return allKeys;
    }

    class Node{
        String key;
        int value;
        Node next;

        public Node(String key, int value) {
            this.key = key;
            this.value = value;
        }
    }

    public void printTable(){
        for(int i = 0 ; i < dataMap.length ; i++){
            System.out.println(i +" : ");
            Node temp = dataMap[i];
            while(temp != null){
                System.out.println("The key = "+ temp.key +" :: value = "+ temp.value);
                temp = temp.next;
            }
        }
    }
}
