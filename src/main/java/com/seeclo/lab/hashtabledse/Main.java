package com.seeclo.lab.hashtabledse;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        HashTable hashTable = new HashTable();

        hashTable.printTable();
        hashTable.set("nitesh", 5);
        hashTable.set("tejeswini", 1);
        hashTable.set("shanthi", 20);
        hashTable.set("narendra", 23);
        hashTable.set("shwetha", 8);
        hashTable.printTable();
        System.out.println("The value of kk :: "+hashTable.get("kk"));
        System.out.println("The value of nitesh :: "+hashTable.get("nitesh"));
        System.out.println("The list of keys");
        List<String> keys = hashTable.keys();
        for(String key : keys){
            System.out.println(key);
        }
    }
}
